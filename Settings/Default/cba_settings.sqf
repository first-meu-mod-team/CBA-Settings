// 1st MEU Aux Mod
force force MEU_Bubble_Deploy_Time = 5;
force force MEU_Bubble_Duration = 240;
force force MEU_Common_Debug = false;
force force MEU_DeployableShield_Duration = 180;
force force MEU_DeployableShield_Time = 3;
force force MEU_EX_Afterburner_Enable = true;
force force MEU_Frigate_Launch_Delay = 30;
force force MEU_Frigate_Move_Duration = 60;
force force MEU_Frigate_Turn_Duration = 15;
force force MEU_Frigate_Vehicles = "Optre_M12_FAV_APC_MED,Optre_M12_FAV_APC,Optre_M813_TT,Optre_M12_FAV,Optre_M12_LRV,Optre_M12A1_LRV,Optre_M12G1_LRV,Optre_M12R_AA,Optre_m914_RV,1st_meu_vulcan_repair_vic,MEU_M412_IFV_UNSC,MEU_Leopard_APC,MEU_IFV_A,MEU_SCORPION,MEU_M808B2,1st_meu_oryx";
force force MEU_Grunt_AI_Flee_Chance = 0.25;
force force MEU_Grunt_AI_Grenade_Charge_Chance = 0.25;
force force MEU_HEV_Atmosphere_End = "2000";
force force MEU_HEV_Atmosphere_Start = "3000";
force force MEU_HEV_Chute_Detach = "500,200,100";
force force MEU_HEV_Chute_Open = "1000,500,300";
force force MEU_HEV_Chute_Velocity = 0;
force force MEU_HEV_Debug_Arrow = false;
force force MEU_HEV_Eject_Delay = 0;
force force MEU_HEV_Eject_Distance = 3;
force force MEU_HEV_Explosion_Class = "MEU_HEV_Landing_Explosive";
force force MEU_HEV_Explosion_Enabled = false;
force force MEU_HEV_Explosion_Simulation_Delay = 0.5;
force force MEU_HEV_Landed_Altitude = 3;
force force MEU_HEV_Start_Heights = "5000,4500,4000";
force force MEU_HEV_Thruster_Limit = false;
force force MEU_HEV_Thruster_Max_Velocity = 20;
force force MEU_HEV_Thruster_Min_Height = 500;
force force MEU_HEV_Thruster_Uses = 5;
force force MEU_HEV_Thruster_Velocity = 5;
force force MEU_Slipspace_Offset_CCS_Battle_Cruiser = 180;
force force MEU_Slipspace_Offset_Default = 180;
force force MEU_Slipspace_Offset_Frigate = 180;
force force MEU_Stealth_AI_Decloak_Distance = 20;

// ACE Advanced Ballistics
force force ace_advanced_ballistics_ammoTemperatureEnabled = false;
force force ace_advanced_ballistics_barrelLengthInfluenceEnabled = false;
force force ace_advanced_ballistics_bulletTraceEnabled = false;
force force ace_advanced_ballistics_enabled = false;
force force ace_advanced_ballistics_muzzleVelocityVariationEnabled = false;
force force ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Throwing
ace_advanced_throwing_enabled = true;
force force ace_advanced_throwing_enablePickUp = true;
force force ace_advanced_throwing_enablePickUpAttached = true;
ace_advanced_throwing_showMouseControls = true;
ace_advanced_throwing_showThrowArc = true;

// ACE Advanced Vehicle Damage
force force ace_vehicle_damage_enableCarDamage = false;
force force ace_vehicle_damage_enabled = false;
force force ace_vehicle_damage_removeAmmoDuringCookoff = true;

// ACE Arsenal
force force ace_arsenal_allowDefaultLoadouts = true;
force force ace_arsenal_allowSharedLoadouts = true;
ace_arsenal_camInverted = false;
force force ace_arsenal_enableIdentityTabs = true;
ace_arsenal_enableModIcons = true;
force ace_arsenal_EnableRPTLog = false;
ace_arsenal_fontHeight = 4.5;

// ACE Artillery
force force ace_artillerytables_advancedCorrections = true;
force force ace_artillerytables_disableArtilleryComputer = false;
force force ace_mk6mortar_airResistanceEnabled = true;
force force ace_mk6mortar_allowCompass = true;
force force ace_mk6mortar_allowComputerRangefinder = true;
force force ace_mk6mortar_useAmmoHandling = true;

// ACE Captives
force force ace_captives_allowHandcuffOwnSide = true;
force force ace_captives_allowSurrender = true;
force force ace_captives_requireSurrender = 2;
force force ace_captives_requireSurrenderAi = false;

// ACE Casings
ace_casings_enabled = true;
ace_casings_maxCasings = 250;

// ACE Common
force force ace_common_allowFadeMusic = true;
force force ace_common_checkPBOsAction = 0;
force force ace_common_checkPBOsCheckAll = true;
force force ace_common_checkPBOsWhitelist = "[""""""""A3PE"""""""",""""""""kka3_TFAR_Animations"""""""",""""""""compositions_a3"""""""",""""""""jsrs_soundmod_complete_edition_soundfiles"""""""",""""""""jsrs_soundmod_complete_edition"""""""",""""""""jsrs_soundmod_boats"""""""",""""""""jsrs_soundmod_sonic_cracks"""""""",""""""""jsrs_soundmod_weapons"""""""",""""""""jsrs_soundmod_environment"""""""",""""""""jsrs_soundmod_explosions"""""""",""""""""jsrs_soundmod_framework"""""""",""""""""jsrs_soundmod_landvehicles"""""""",""""""""jsrs_soundmod_movement"""""""",""""""""jsrs_soundmod_bullethits"""""""",""""""""jsrs_soundmod_helicopters"""""""",""""""""jsrs_soundmod_rhs_afrf_weapons"""""""",""""""""jsrs_soundmod_cfg_rhs_afrf_weapons"""""""",""""""""jsrs_soundmod_cfg_rhs_afrf_vehicles"""""""",""""""""jsrs_soundmod_cfg_rhs_afrf_air_vehicles"""""""",""""""""OPTRE_JSRS"""""""",""""""""PH_TacReady"""""""",""""""""ZEI"""""""",""""""""ZECCUP"""""""",""""""""3denEnhanced"""""""",""""""""athena"""""""",""""""""DNI_ZeusFPSMonitor"""""""",""""""""PH_TacSwap"""""""",""""""""Vile_HUD"""""""",""""""""UTGX_Compass"""""""",""""""""Darth_Potatos_Building_Templates"""""""",""""""""DXX_COMPS"""""""",""""""""composition_tool"""""""",""""""""GF_ReColor"""""""",""""""""xru_VTOL_PIP"""""""",""""""""xru_VTOL_PIP_Laser"""""""",""""""""DIS_Enhanced_Gps"""""""",""""""""PLP_MapTools"""""""",""""""""PLP_VTOLThrust"""""""",""""""""dwyl_main"""""""",""""""""simplesuppress_main"""""""",""""""""simplesuppress_suppress"""""""",""""""""A3TI"""""""",""""""""CW_9LINERS_AND_NOTEPAD"""""""",""""""""DynamicViewDistance"""""""",""""""""field_ftmarkers"""""""",""""""""hal_AutoPilot"""""""",""""""""UAV_Turret_Markers"""""""",""""""""CHTR_TFAR_Setter"""""""",""""""""UPSL_aime"""""""",""""""""UPSL_aime_change_ammo"""""""",""""""""UPSL_aime_group"""""""",""""""""UPSL_aime_inventory"""""""",""""""""UPSL_aime_uav_terminal"""""""",""""""""UPSL_aime_vehicle_controls"""""""",""""""""UPSL_aime_vehicle_seats"""""""",""""""""WP_Mod"""""""",""""""""asw_main"""""""",""""""""DIS_enhanced_map_ace"""""""",""""""""ASAAYU_ACE_MEDICAL_ASSISTANT"""""""",""""""""MIRA_Vehicle_Medical"""""""",""PLP_VTOLHoverController"",""pandora"",pandoracustom_data"",""pandoraplants_data"",""plp_markers"",""wnz_napalm_bombs"",""fsn_freestyle_nuke"",amp_breaching_charge,""amp_door_wedge"",""phan""]";
ace_common_displayTextColor = [0,0,0,0.1];
ace_common_displayTextFontColor = [1,1,1,1];
ace_common_epilepsyFriendlyMode = false;
ace_common_progressBarInfo = 2;
ace_common_settingFeedbackIcons = 1;
ace_common_settingProgressBarLocation = 0;

// ACE Cook off
force force ace_cookoff_ammoCookoffDuration = 0.1;
force force ace_cookoff_enable = 0;
force force ace_cookoff_enableAmmobox = false;
force force ace_cookoff_enableAmmoCookoff = true;
force force ace_cookoff_enableFire = true;
force force ace_cookoff_probabilityCoef = 0.2;

// ACE Crew Served Weapons
force force ace_csw_ammoHandling = 1;
force force ace_csw_defaultAssemblyMode = true;
force force ace_csw_dragAfterDeploy = true;
force force ace_csw_handleExtraMagazines = false;
force force ace_csw_handleExtraMagazinesType = 0;
force force ace_csw_progressBarTimeCoefficent = 1;

// ACE Dragging
ace_dragging_dragAndFire = true;

// ACE Explosives
ace_explosives_customTimerDefault = 30;
force force ace_explosives_customTimerMax = 900;
force force ace_explosives_customTimerMin = 5;
force force ace_explosives_explodeOnDefuse = true;
force force ace_explosives_punishNonSpecialists = true;
force force ace_explosives_requireSpecialist = false;

// ACE Field Rations
force force acex_field_rations_affectAdvancedFatigue = true;
force force acex_field_rations_enabled = false;
acex_field_rations_hudShowLevel = 0;
acex_field_rations_hudTransparency = -1;
acex_field_rations_hudType = 0;
force force acex_field_rations_hungerSatiated = 1;
force force acex_field_rations_terrainObjectActions = true;
force force acex_field_rations_thirstQuenched = 1;
force force acex_field_rations_timeWithoutFood = 2;
force force acex_field_rations_timeWithoutWater = 2;
force force acex_field_rations_waterSourceActions = 2;

// ACE Fire
force force ace_fire_dropWeapon = 1;
force force ace_fire_enabled = true;
force force ace_fire_enableFlare = false;
ace_fire_enableScreams = true;

// ACE Fortify
force force ace_fortify_markObjectsOnMap = 1;
force force ace_fortify_timeCostCoefficient = 1;
force force ace_fortify_timeMin = 1.5;
acex_fortify_settingHint = 2;

// ACE Fragmentation Simulation
force force ace_frag_enabled = true;
force force ace_frag_maxTrack = 10;
force force ace_frag_maxTrackPerFrame = 10;
force force ace_frag_reflectionsEnabled = false;
force force ace_frag_spallEnabled = false;

// ACE G-Forces
force force ace_gforces_coef = 0.1;
force force ace_gforces_enabledFor = 2;

// ACE Goggles
force force ace_goggles_effects = 2;
force force ace_goggles_showClearGlasses = true;
ace_goggles_showInThirdPerson = true;

// ACE Grenades
force force ace_grenades_convertExplosives = true;

// ACE Headless
force force acex_headless_delay = 15;
force force acex_headless_enabled = false;
force force acex_headless_endMission = 0;
force force acex_headless_log = false;
force force acex_headless_transferLoadout = 0;

// ACE Hearing
force force ace_hearing_autoAddEarplugsToUnits = false;
force force ace_hearing_disableEarRinging = false;
force force ace_hearing_earplugsVolume = 0;
force force ace_hearing_enableCombatDeafness = false;
force force ace_hearing_enabledForZeusUnits = true;
force force ace_hearing_unconsciousnessVolume = 0.25;

// ACE Interaction
force force ace_interaction_disableNegativeRating = true;
force force ace_interaction_enableGroupRenaming = true;
force force ace_interaction_enableMagazinePassing = true;
force force ace_interaction_enableTeamManagement = true;
ace_interaction_enableWeaponAttachments = true;
force force ace_interaction_interactWithTerrainObjects = false;

// ACE Interaction Menu
ace_gestures_showOnInteractionMenu = 2;
ace_interact_menu_actionOnKeyRelease = true;
force force ace_interact_menu_addBuildingActions = true;
ace_interact_menu_alwaysUseCursorInteraction = false;
ace_interact_menu_alwaysUseCursorSelfInteraction = true;
ace_interact_menu_colorShadowMax = [0,0,0,1];
ace_interact_menu_colorShadowMin = [0,0,0,0.25];
ace_interact_menu_colorTextMax = [1,1,1,1];
ace_interact_menu_colorTextMin = [1,1,1,0.25];
ace_interact_menu_consolidateSingleChild = false;
ace_interact_menu_cursorKeepCentered = false;
ace_interact_menu_cursorKeepCenteredSelfInteraction = false;
ace_interact_menu_menuAnimationSpeed = 0;
ace_interact_menu_menuBackground = 0;
ace_interact_menu_menuBackgroundSelf = 0;
ace_interact_menu_selectorColor = [1,0,0];
ace_interact_menu_shadowSetting = 2;
ace_interact_menu_textSize = 2;
ace_interact_menu_useListMenu = true;
ace_interact_menu_useListMenuSelf = false;

// ACE Interaction Menu (Self) - More
ace_interact_menu_more__ACE_CheckAirTemperature = false;
ace_interact_menu_more__ace_csw_deploy = false;
ace_interact_menu_more__ACE_Equipment = false;
ace_interact_menu_more__ACE_Explosives = false;
ace_interact_menu_more__ace_field_rations = false;
ace_interact_menu_more__ace_fortify = false;
ace_interact_menu_more__ace_gestures = false;
ace_interact_menu_more__ace_intelitems = false;
ace_interact_menu_more__ACE_MapFlashlight = false;
ace_interact_menu_more__ACE_MapGpsHide = false;
ace_interact_menu_more__ACE_MapGpsShow = false;
ace_interact_menu_more__ACE_MapTools = false;
ace_interact_menu_more__ACE_MCC_CommanderTab = false;
ace_interact_menu_more__ACE_MCC_doorinteraction = false;
ace_interact_menu_more__ACE_MCC_DropAmmobox = false;
ace_interact_menu_more__ACE_MCC_miniGameDefuse = false;
ace_interact_menu_more__ACE_MCC_SQLMenu = false;
ace_interact_menu_more__ACE_MCC_survivalInteraction = false;
ace_interact_menu_more__ACE_Medical = false;
ace_interact_menu_more__ACE_Medical_Menu = false;
ace_interact_menu_more__ACE_MoveRallypoint = false;
ace_interact_menu_more__ACE_RepackMagazines = false;
ace_interact_menu_more__ace_sandbag_place = false;
ace_interact_menu_more__ACE_StartSurrenderingSelf = false;
ace_interact_menu_more__ACE_StopEscortingSelf = false;
ace_interact_menu_more__ACE_StopSurrenderingSelf = false;
ace_interact_menu_more__ACE_Tags = false;
ace_interact_menu_more__ACE_TeamManagement = false;
ace_interact_menu_more__ace_zeus_create = false;
ace_interact_menu_more__ace_zeus_delete = false;
ace_interact_menu_more__acex_sitting_Stand = false;
ace_interact_menu_more__DotMarkerCreate = false;

// ACE Interaction Menu (Self) - Move to Root
ace_interact_menu_moveToRoot__ACE_Equipment__ace_atragmx_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_attach_Attach = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_attach_Detach = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_CheckDogtags = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_Chemlights = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_dagr_menu = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_dagr_menu__ace_dagr_toggle = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_goggles_wipeGlasses = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_status = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponOff = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponSwap = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponTo = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_huntir_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_interaction_weaponAttachments = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open__ace_kestrel4500_hide = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open__ace_kestrel4500_show = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_marker_flags = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure__ace_microdagr_close = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure__ace_microdagr_show = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_activate = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_connectHeadphones = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_deactivate = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_disconnectHeadphones = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_mk6mortar_rangetable = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CheckTemperature = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CheckTemperatureSpareBarrels = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CoolWeaponWithItem = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_SwapBarrel = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_UnJam = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_PutInEarplugs = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open__ace_rangecard_makeCopy = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open__ace_rangecard_openCopy = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_reload_checkAmmo = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_RemoveEarplugs = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_scopes_adjustZero = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_scopes_resetZero = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_spottingscope_place = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_TacticalLadders = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__ace_trenches_digEnvelopeBig = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__ace_trenches_digEnvelopeSmall = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeGiant = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeLongNameEmplacment = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeShort = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeVehicle = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_tripod_place = false;
ace_interact_menu_moveToRoot__ACE_Equipment__AMP_DeployDoorWedge = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Rover_Open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__itc_land_tablet_fdc = false;
ace_interact_menu_moveToRoot__ACE_Equipment__itc_land_tablet_spg = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AL6_B = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2_B = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2_I = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2_O = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2e_B = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2e_I = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2e_O = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2i_B = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2i_I = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_AR2i_O = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_RemoteGLTD_B = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ITC_Land_Unpack__ITC_Land_Unpack_RemoteGLTD_BW = false;
ace_interact_menu_moveToRoot__ACE_Equipment__MRH_Ace_SATCOM_ANTENNA = false;
ace_interact_menu_moveToRoot__ACE_Equipment__MRH_Ace_USE_UTD = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_onBack = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_onChest = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_swap = false;
ace_interact_menu_moveToRoot__ACE_Explosives__ACE_Cellphone = false;
ace_interact_menu_moveToRoot__ACE_Explosives__ACE_Place = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Advance = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_CeaseFire = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Cover = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Engage = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Follow = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Forward = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Freeze = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Go = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Hold = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Point = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Regroup = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Stop = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Up = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Warning = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlignCompass = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlignNorth = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsHide = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsShowNormal = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsShowSmall = false;
ace_interact_menu_moveToRoot__ACE_MCC_doorinteraction__ACE_MCC_door_bang = false;
ace_interact_menu_moveToRoot__ACE_MCC_doorinteraction__ACE_MCC_door_breach = false;
ace_interact_menu_moveToRoot__ACE_MCC_doorinteraction__ACE_MCC_door_check = false;
ace_interact_menu_moveToRoot__ACE_MCC_doorinteraction__ACE_MCC_door_lock = false;
ace_interact_menu_moveToRoot__ACE_MCC_doorinteraction__ACE_MCC_door_mirror = false;
ace_interact_menu_moveToRoot__ACE_MCC_doorinteraction__ACE_MCC_door_unlock = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_AA = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_AT = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_Bunker = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_FOB = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_GMG = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_GMGH = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_HMG = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_HMGH = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_mortar = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callConstruct__ACE_MCC_wall = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport__ACE_MCC_ammo = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport__ACE_MCC_areaAttack = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport__ACE_MCC_CAS = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport__ACE_MCC_fuel = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport__ACE_MCC_medic = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport__ACE_MCC_repair = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport__ACE_MCC_Support = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_callSupport__ACE_MCC_transport = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_openSQLPDA = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_air = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_armor = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_art = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_infantry = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_infantry__fire_team = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_infantry__Platoon = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_infantry__Section = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_infantry__Squad = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_Mine = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_motorized = false;
ace_interact_menu_moveToRoot__ACE_MCC_SQLMenu__ACE_MCC_spotEnemy__ACE_MCC_naval = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_ArmLeft = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_ArmRight = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Head = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_LegLeft = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_LegRight = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Torso = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Torso__TriageCard = false;
ace_interact_menu_moveToRoot__ACE_Medical__dev_enzymeCapsule = false;
ace_interact_menu_moveToRoot__ACE_Medical__dev_enzymeCapsule_refined = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_BecomeLeader = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamBlue = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamGreen = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamRed = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamYellow = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_LeaveGroup = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_LeaveTeam = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_RenameGroup = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__diwako_dui_buddy_buddy_action_team_remove = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__CCPMarkerSelf = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__CCPMarkerSelf__DotMarkerSelfDeleteCCP = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__DotMarkerSelfDelete = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__DZMarkerSelf = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__DZMarkerSelf__DotMarkerSelfDeleteDZ = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__LZMarkerSelf = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__LZMarkerSelf__DotMarkerSelfDeleteLZ = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__RPMarkerSelf = false;
ace_interact_menu_moveToRoot__DotMarkerCreate__RPMarkerSelf__DotMarkerSelfDeleteRP = false;

// ACE Logistics
force force ace_cargo_enable = true;
ace_cargo_enableRename = true;
force force ace_cargo_loadTimeCoefficient = 5;
ace_cargo_openAfterUnload = 0;
force force ace_cargo_paradropTimeCoefficent = 2.5;
force force ace_rearm_distance = 20;
force force ace_rearm_level = 0;
force force ace_rearm_supply = 0;
force force ace_refuel_hoseLength = 12;
force force ace_refuel_rate = 1;
force force ace_repair_addSpareParts = true;
force force ace_repair_autoShutOffEngineWhenStartingRepair = true;
force force ace_repair_consumeItem_toolKit = 0;
ace_repair_displayTextOnRepair = true;
force force ace_repair_engineerSetting_fullRepair = 2;
force force ace_repair_engineerSetting_repair = 1;
force force ace_repair_engineerSetting_wheel = 0;
force force ace_repair_fullRepairLocation = 3;
force force ace_repair_fullRepairRequiredItems = ["ace_repair_anyToolKit"];
force force ace_repair_locationsBoostTraining = false;
force force ace_repair_miscRepairRequiredItems = ["ace_repair_anyToolKit"];
force force ace_repair_repairDamageThreshold = 0.6;
force force ace_repair_repairDamageThreshold_engineer = 0.4;
force force ace_repair_wheelRepairRequiredItems = [];

// ACE Magazine Repack
ace_magazinerepack_repackAnimation = true;
ace_magazinerepack_repackLoadedMagazines = true;
force force ace_magazinerepack_timePerAmmo = 1;
force force ace_magazinerepack_timePerBeltLink = 6;
force force ace_magazinerepack_timePerMagazine = 1;

// ACE Map
force force ace_map_BFT_Enabled = true;
force force ace_map_BFT_HideAiGroups = false;
force force ace_map_BFT_Interval = 15;
force force ace_map_BFT_ShowPlayerNames = false;
force force ace_map_DefaultChannel = -1;
force force ace_map_mapGlow = true;
force force ace_map_mapIllumination = true;
force force ace_map_mapLimitZoom = false;
force force ace_map_mapShake = true;
force force ace_map_mapShowCursorCoordinates = true;
force force ace_markers_moveRestriction = 0;
ace_markers_timestampEnabled = true;
ace_markers_timestampFormat = "HH:MM";
ace_markers_timestampHourFormat = 24;

// ACE Map Gestures
ace_map_gestures_allowCurator = true;
ace_map_gestures_allowSpectator = true;
ace_map_gestures_briefingMode = 0;
ace_map_gestures_defaultColor = [1,0.88,0,0.7];
ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
force force ace_map_gestures_enabled = true;
force force ace_map_gestures_interval = 0.03;
force force ace_map_gestures_maxRange = 7;
force force ace_map_gestures_maxRangeCamera = 14;
ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];
force force ace_map_gestures_onlyShowFriendlys = false;

// ACE Map Tools
ace_maptools_drawStraightLines = true;
ace_maptools_rotateModifierKey = 1;

// ACE Medical
force force ace_medical_ai_enabledFor = 2;
force force ace_medical_AIDamageThreshold = 1;
force force ace_medical_bleedingCoefficient = 0.9;
force force ace_medical_blood_bloodLifetime = 420;
force force ace_medical_blood_enabledFor = 2;
force force ace_medical_blood_maxBloodObjects = 200;
force force ace_medical_deathChance = 0.9;
force force ace_medical_enableVehicleCrashes = true;
force force ace_medical_fatalDamageSource = 0;
ace_medical_feedback_bloodVolumeEffectType = 2;
ace_medical_feedback_enableHUDIndicators = true;
ace_medical_feedback_painEffectType = 1;
force force ace_medical_fractureChance = 0.502539;
force force ace_medical_fractures = 1;
ace_medical_gui_bloodLossColor_0 = [1,1,1,1];
ace_medical_gui_bloodLossColor_1 = [1,0.95,0.64,1];
ace_medical_gui_bloodLossColor_2 = [1,0.87,0.46,1];
ace_medical_gui_bloodLossColor_3 = [1,0.8,0.33,1];
ace_medical_gui_bloodLossColor_4 = [1,0.72,0.24,1];
ace_medical_gui_bloodLossColor_5 = [1,0.63,0.15,1];
ace_medical_gui_bloodLossColor_6 = [1,0.54,0.08,1];
ace_medical_gui_bloodLossColor_7 = [1,0.43,0.02,1];
ace_medical_gui_bloodLossColor_8 = [1,0.3,0,1];
ace_medical_gui_bloodLossColor_9 = [1,0,0,1];
ace_medical_gui_damageColor_0 = [1,1,1,1];
ace_medical_gui_damageColor_1 = [0.75,0.95,1,1];
ace_medical_gui_damageColor_2 = [0.62,0.86,1,1];
ace_medical_gui_damageColor_3 = [0.54,0.77,1,1];
ace_medical_gui_damageColor_4 = [0.48,0.67,1,1];
ace_medical_gui_damageColor_5 = [0.42,0.57,1,1];
ace_medical_gui_damageColor_6 = [0.37,0.47,1,1];
ace_medical_gui_damageColor_7 = [0.31,0.36,1,1];
ace_medical_gui_damageColor_8 = [0.22,0.23,1,1];
ace_medical_gui_damageColor_9 = [0,0,1,1];
ace_medical_gui_enableActions = 0;
force force ace_medical_gui_enableMedicalMenu = 1;
ace_medical_gui_enableSelfActions = true;
ace_medical_gui_interactionMenuShowTriage = 1;
force force ace_medical_gui_maxDistance = 3;
force force ace_medical_gui_openAfterTreatment = true;
force force ace_medical_gui_showBloodlossEntry = true;
force force ace_medical_ivFlowRate = 4.2;
force force ace_medical_limping = 1;
force force ace_medical_painCoefficient = 1;
force force ace_medical_painUnconsciousChance = 0.1;
force force ace_medical_playerDamageThreshold = 0.5;
force force ace_medical_spontaneousWakeUpChance = 0.95;
force force ace_medical_spontaneousWakeUpEpinephrineBoost = 4;
force force ace_medical_statemachine_AIUnconsciousness = true;
force force ace_medical_statemachine_cardiacArrestBleedoutEnabled = true;
force force ace_medical_statemachine_cardiacArrestTime = 120;
force force ace_medical_statemachine_fatalInjuriesAI = 0;
force force ace_medical_statemachine_fatalInjuriesPlayer = 0;
force force ace_medical_treatment_advancedBandages = 2;
force force ace_medical_treatment_advancedDiagnose = 1;
force force ace_medical_treatment_advancedMedication = true;
force force ace_medical_treatment_allowBodyBagUnconscious = true;
force force ace_medical_treatment_allowLitterCreation = true;
force force ace_medical_treatment_allowSelfIV = 1;
force force ace_medical_treatment_allowSelfPAK = 1;
force force ace_medical_treatment_allowSelfStitch = 1;
force force ace_medical_treatment_allowSharedEquipment = 0;
force force ace_medical_treatment_clearTrauma = 1;
force force ace_medical_treatment_consumePAK = 0;
force force ace_medical_treatment_consumeSurgicalKit = 0;
force force ace_medical_treatment_convertItems = 2;
force force ace_medical_treatment_cprSuccessChanceMax = 0.9;
force force ace_medical_treatment_cprSuccessChanceMin = 0.8;
force force ace_medical_treatment_holsterRequired = 0;
force force ace_medical_treatment_litterCleanupDelay = 300;
force force ace_medical_treatment_locationEpinephrine = 0;
force force ace_medical_treatment_locationIV = 0;
force force ace_medical_treatment_locationPAK = 3;
force force ace_medical_treatment_locationsBoostTraining = true;
force force ace_medical_treatment_locationSurgicalKit = 0;
force force ace_medical_treatment_maxLitterObjects = 50;
force force ace_medical_treatment_medicEpinephrine = 0;
force force ace_medical_treatment_medicIV = 1;
force force ace_medical_treatment_medicPAK = 1;
force force ace_medical_treatment_medicSurgicalKit = 1;
force force ace_medical_treatment_timeCoefficientPAK = 1;
force force ace_medical_treatment_treatmentTimeAutoinjector = 5;
force force ace_medical_treatment_treatmentTimeBodyBag = 15;
force force ace_medical_treatment_treatmentTimeCPR = 15;
force force ace_medical_treatment_treatmentTimeIV = 10;
force force ace_medical_treatment_treatmentTimeSplint = 7;
force force ace_medical_treatment_treatmentTimeTourniquet = 7;
force force ace_medical_treatment_woundReopenChance = 1;
force force ace_medical_treatment_woundStitchTime = 5;

// ACE Name Tags
force force ace_nametags_ambientBrightnessAffectViewDist = 1;
ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
ace_nametags_nametagColorBlue = [0.67,0.67,1,1];
ace_nametags_nametagColorGreen = [0.67,1,0.67,1];
ace_nametags_nametagColorMain = [1,1,1,1];
ace_nametags_nametagColorRed = [1,0.67,0.67,1];
ace_nametags_nametagColorYellow = [1,1,0.67,1];
force force ace_nametags_playerNamesMaxAlpha = 0.8;
force force ace_nametags_playerNamesViewDistance = 5;
force force ace_nametags_showCursorTagForVehicles = false;
ace_nametags_showNamesForAI = false;
ace_nametags_showPlayerNames = 5;
force force ace_nametags_showPlayerRanks = false;
ace_nametags_showSoundWaves = 1;
ace_nametags_showVehicleCrewInfo = true;
ace_nametags_tagSize = 1;

// ACE Nightvision
force force ace_nightvision_aimDownSightsBlur = 0;
force force ace_nightvision_disableNVGsWithSights = false;
force force ace_nightvision_effectScaling = 0;
force force ace_nightvision_fogScaling = 0;
force force ace_nightvision_noiseScaling = 0;
force force ace_nightvision_shutterEffects = true;

// ACE Overheating
force force ace_overheating_cookoffCoef = 1;
force force ace_overheating_coolingCoef = 1;
ace_overheating_displayTextOnJam = false;
force force ace_overheating_enabled = false;
force force ace_overheating_heatCoef = 1;
force force ace_overheating_jamChanceCoef = 1;
force force ace_overheating_overheatingDispersion = false;
force force ace_overheating_overheatingRateOfFire = false;
ace_overheating_particleEffectsAndDispersionDistance = 3000;
force force ace_overheating_showParticleEffects = false;
ace_overheating_showParticleEffectsForEveryone = false;
force force ace_overheating_suppressorCoef = 1;
force force ace_overheating_unJamFailChance = 0;
force force ace_overheating_unJamOnreload = false;
force force ace_overheating_unJamOnSwapBarrel = false;

// ACE Pointing
force force ace_finger_enabled = true;
ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
ace_finger_indicatorForSelf = true;
force force ace_finger_maxRange = 4;
force force ace_finger_proximityScaling = false;
force force ace_finger_sizeCoef = 1;

// ACE Pylons
force force ace_pylons_enabledForZeus = true;
force force ace_pylons_enabledFromAmmoTrucks = true;
force force ace_pylons_rearmNewPylons = false;
force force ace_pylons_requireEngineer = false;
force force ace_pylons_requireToolkit = true;
force force ace_pylons_searchDistance = 15;
force force ace_pylons_timePerPylon = 5;

// ACE Quick Mount
force force ace_quickmount_distance = 3;
force force ace_quickmount_enabled = true;
ace_quickmount_enableMenu = 3;
ace_quickmount_priority = 0;
force force ace_quickmount_speed = 18;

// ACE Respawn
force force ace_respawn_removeDeadBodiesDisconnected = true;
force force ace_respawn_savePreDeathGear = true;

// ACE Scopes
force force ace_scopes_correctZeroing = true;
force force ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
force force ace_scopes_defaultZeroRange = 100;
force force ace_scopes_enabled = true;
force force ace_scopes_forceUseOfAdjustmentTurrets = false;
force force ace_scopes_overwriteZeroRange = false;
force force ace_scopes_simplifiedZeroing = false;
ace_scopes_useLegacyUI = false;
force force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force force ace_scopes_zeroReferenceHumidity = 0;
force force ace_scopes_zeroReferenceTemperature = 15;

// ACE Sitting
force force acex_sitting_enable = true;

// ACE Spectator
force force ace_spectator_enableAI = false;
ace_spectator_maxFollowDistance = 5;
force force ace_spectator_restrictModes = 0;
force force ace_spectator_restrictVisions = 0;

// ACE Switch Units
force force ace_switchunits_enableSafeZone = true;
force force ace_switchunits_enableSwitchUnits = false;
force force ace_switchunits_safeZoneRadius = 100;
force force ace_switchunits_switchToCivilian = false;
force force ace_switchunits_switchToEast = false;
force force ace_switchunits_switchToIndependent = false;
force force ace_switchunits_switchToWest = false;

// ACE Trenches
force force ace_trenches_bigEnvelopeDigDuration = 55;
force force ace_trenches_bigEnvelopeRemoveDuration = 15;
force force ace_trenches_smallEnvelopeDigDuration = 50;
force force ace_trenches_smallEnvelopeRemoveDuration = 42;

// ACE Uncategorized
force force ace_fastroping_requireRopeItems = false;
force force ace_gunbag_swapGunbagEnabled = true;
force force ace_hitreactions_minDamageToTrigger = 0.15;
ace_inventory_inventoryDisplaySize = 0;
force force ace_laser_dispersionCount = 2;
force force ace_laser_showLaserOnMap = 1;
force force ace_marker_flags_placeAnywhere = false;
force force ace_microdagr_mapDataAvailable = 2;
force force ace_microdagr_waypointPrecision = 3;
force force ace_noradio_enabled = true;
ace_optionsmenu_showNewsOnMainMenu = true;
force force ace_overpressure_distanceCoefficient = 0.5;
force force ace_parachute_failureChance = 0.01;
ace_parachute_hideAltimeter = true;
ace_tagging_quickTag = 1;

// ACE User Interface
force force ace_ui_allowSelectiveUI = true;
force force ace_ui_ammoCount = false;
ace_ui_ammoType = true;
ace_ui_commandMenu = false;
force force ace_ui_enableSpeedIndicator = true;
ace_ui_firingMode = true;
force force ace_ui_groupBar = false;
ace_ui_gunnerAmmoCount = true;
ace_ui_gunnerAmmoType = true;
ace_ui_gunnerFiringMode = true;
ace_ui_gunnerLaunchableCount = true;
ace_ui_gunnerLaunchableName = true;
ace_ui_gunnerMagCount = true;
ace_ui_gunnerWeaponLowerInfoBackground = true;
ace_ui_gunnerWeaponName = true;
ace_ui_gunnerWeaponNameBackground = true;
ace_ui_gunnerZeroing = true;
ace_ui_hideDefaultActionIcon = false;
ace_ui_magCount = true;
ace_ui_soldierVehicleWeaponInfo = true;
ace_ui_staminaBar = true;
ace_ui_stance = true;
ace_ui_throwableCount = true;
ace_ui_throwableName = true;
ace_ui_vehicleAltitude = true;
ace_ui_vehicleCompass = true;
ace_ui_vehicleDamage = true;
ace_ui_vehicleFuelBar = true;
ace_ui_vehicleInfoBackground = true;
ace_ui_vehicleName = true;
ace_ui_vehicleNameBackground = true;
ace_ui_vehicleRadar = true;
ace_ui_vehicleSpeed = true;
ace_ui_weaponLowerInfoBackground = true;
ace_ui_weaponName = true;
ace_ui_weaponNameBackground = true;
ace_ui_zeroing = true;

// ACE Vehicle Lock
force force ace_vehiclelock_defaultLockpickStrength = 10;
force force ace_vehiclelock_lockVehicleInventory = false;
force force ace_vehiclelock_vehicleStartingLockState = -1;

// ACE Vehicles
ace_vehicles_hideEjectAction = true;
force force ace_vehicles_keepEngineRunning = true;
ace_vehicles_speedLimiterStep = 5;
force force ace_viewports_enabled = true;

// ACE View Distance Limiter
force force ace_viewdistance_enabled = false;
force force ace_viewdistance_limitViewDistance = 12000;
ace_viewdistance_objectViewDistanceCoeff = 0;
ace_viewdistance_viewDistanceAirVehicle = 0;
ace_viewdistance_viewDistanceLandVehicle = 0;
ace_viewdistance_viewDistanceOnFoot = 0;

// ACE View Restriction
force force acex_viewrestriction_mode = 0;
force force acex_viewrestriction_modeSelectiveAir = 0;
force force acex_viewrestriction_modeSelectiveFoot = 0;
force force acex_viewrestriction_modeSelectiveLand = 0;
force force acex_viewrestriction_modeSelectiveSea = 0;
acex_viewrestriction_preserveView = false;

// ACE Volume
acex_volume_enabled = false;
acex_volume_fadeDelay = 1;
acex_volume_lowerInVehicles = false;
acex_volume_reduction = 5;
acex_volume_remindIfLowered = false;
acex_volume_showNotification = true;

// ACE Weapons
ace_common_persistentLaserEnabled = false;
force force ace_laserpointer_enabled = true;
ace_reload_displayText = true;
ace_reload_showCheckAmmoSelf = false;
ace_weaponselect_displayText = true;

// ACE Weather
force force ace_weather_enabled = true;
ace_weather_showCheckAirTemperature = true;
force force ace_weather_updateInterval = 300;
force force ace_weather_windSimulation = true;

// ACE Wind Deflection
force force ace_winddeflection_enabled = false;
force force ace_winddeflection_simulationInterval = 0.2;
force force ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
force force ace_zeus_autoAddObjects = true;
force force ace_zeus_canCreateZeus = -1;
force force ace_zeus_radioOrdnance = false;
force force ace_zeus_remoteWind = false;
force force ace_zeus_revealMines = 0;
force force ace_zeus_zeusAscension = true;
force force ace_zeus_zeusBird = false;

// Arma 3 Performance Settings
EnableAIHide = true;
EnableDeadHide = true;
force force EnableHCOverride = false;
EnablePlayerHide = false;
EnableVehicleHide = false;
ForceRenderDistance = 10;
force force HigherQualityAI = false;
force force HigherQualityDead = false;
force force HigherQualityPlayer = false;
force force HigherQualityVehicles = false;

// Community Base Addons
cba_diagnostic_ConsoleIndentType = -1;
force force cba_diagnostic_watchInfoRefreshRate = 0.2;
cba_disposable_dropUsedLauncher = 1;
force force cba_disposable_replaceDisposableLauncher = false;
cba_events_repetitionMode = 1;
force force cba_network_loadoutValidation = 0;
cba_optics_usePipOptics = true;
cba_ui_notifyLifetime = 4;
cba_ui_StorePasswords = 1;

// Crows Zeus Additions
crowsZA_CBA_Setting_pingbox_enabled = true;
crowsZA_CBA_Setting_pingbox_fade_duration = 300;
crowsZA_CBA_Setting_pingbox_fade_enabled = true;
crowsZA_CBA_Setting_pingbox_oldLimit = 600;

// DevourerKing Common
dev_cba_damageMultiplier = 1;
dev_cba_friendly = "[""HeadlessClient_F"", ""VirtualCurator_F"", ""B_VirtualCurator_F"", ""O_VirtualCurator_F"", ""I_VirtualCurator_F"", ""C_VirtualCurator_F""]";
force force dev_cba_friendlySide = false;
force force dev_cba_killswitch = false;
force force dev_zombie_deleteWeapon = true;

// DUI - Squad Radar - Indicators
force diwako_dui_indicators_crew_range_enabled = false;
diwako_dui_indicators_fov_scale = false;
diwako_dui_indicators_icon_buddy = true;
diwako_dui_indicators_icon_leader = true;
diwako_dui_indicators_icon_medic = true;
force diwako_dui_indicators_range = 20;
force diwako_dui_indicators_range_crew = 300;
diwako_dui_indicators_range_scale = false;
diwako_dui_indicators_show = true;
force diwako_dui_indicators_size = 1;
diwako_dui_indicators_style = "standard";
diwako_dui_indicators_useACENametagsRange = true;

// DUI - Squad Radar - Main
diwako_dui_ace_hide_interaction = true;
diwako_dui_colors = "standard";
diwako_dui_font = "RobotoCondensed";
diwako_dui_icon_style = "standard";
diwako_dui_main_hide_dialog = true;
diwako_dui_main_hide_ui_by_default = false;
diwako_dui_main_squadBlue = [0,0,1,1];
diwako_dui_main_squadGreen = [0,1,0,1];
diwako_dui_main_squadMain = [1,1,1,1];
diwako_dui_main_squadRed = [1,0,0,1];
diwako_dui_main_squadYellow = [1,1,0,1];
diwako_dui_main_trackingColor = [0.93,0.26,0.93,1];
diwako_dui_reset_ui_pos = false;

// DUI - Squad Radar - Nametags
diwako_dui_nametags_customRankStyle = "[[""PRIVATE"",""CORPORAL"",""SERGEANT"",""LIEUTENANT"",""CAPTAIN"",""MAJOR"",""COLONEL""],[""Pvt."",""Cpl."",""Sgt."",""Lt."",""Capt."",""Maj."",""Col.""]]";
diwako_dui_nametags_deadColor = [0.2,0.2,0.2,1];
force diwako_dui_nametags_deadRenderDistance = 3.5;
diwako_dui_nametags_drawRank = true;
diwako_dui_nametags_enabled = true;
diwako_dui_nametags_enableFOVBoost = true;
diwako_dui_nametags_enableOcclusion = true;
diwako_dui_nametags_fadeInTime = 0.05;
force diwako_dui_nametags_fadeOutTime = 0.5;
diwako_dui_nametags_fontGroup = "RobotoCondensedLight";
diwako_dui_nametags_fontGroupNameSize = 8;
diwako_dui_nametags_fontName = "RobotoCondensedBold";
diwako_dui_nametags_fontNameSize = 10;
diwako_dui_nametags_groupColor = [1,1,1,1];
diwako_dui_nametags_groupFontShadow = 1;
diwako_dui_nametags_groupNameOtherGroupColor = [0.6,0.85,0.6,1];
diwako_dui_nametags_nameFontShadow = 1;
diwako_dui_nametags_nameOtherGroupColor = [0.2,1,0,1];
diwako_dui_nametags_rankNameStyle = "default";
force diwako_dui_nametags_renderDistance = 40;
diwako_dui_nametags_showUnconAsDead = true;
diwako_dui_nametags_useLIS = true;
diwako_dui_nametags_useSideIsFriendly = true;

// DUI - Squad Radar - Radar
diwako_dui_compass_hide_alone_group = false;
diwako_dui_compass_hide_blip_alone_group = false;
diwako_dui_compass_icon_scale = 1;
diwako_dui_compass_opacity = 1;
diwako_dui_compass_style = ["\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass_limited.paa","\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass.paa"];
diwako_dui_compassRange = 35;
diwako_dui_compassRefreshrate = 0;
diwako_dui_dir_showMildot = false;
diwako_dui_dir_size = 1.25;
diwako_dui_distanceWarning = 3;
diwako_dui_enable_compass = true;
diwako_dui_enable_compass_dir = 1;
diwako_dui_enable_occlusion = false;
diwako_dui_enable_occlusion_cone = 360;
diwako_dui_hudScaling = 1.33333;
diwako_dui_namelist = true;
diwako_dui_namelist_bg = 0;
diwako_dui_namelist_only_buddy_icon = false;
diwako_dui_namelist_size = 1.5396;
diwako_dui_namelist_text_shadow = 2;
diwako_dui_namelist_width = 215;
diwako_dui_radar_ace_finger = true;
force diwako_dui_radar_ace_medic = true;
diwako_dui_radar_compassRangeCrew = 500;
diwako_dui_radar_dir_padding = 25;
diwako_dui_radar_dir_shadow = 2;
diwako_dui_radar_group_by_vehicle = false;
diwako_dui_radar_icon_opacity = 1;
diwako_dui_radar_icon_opacity_no_player = true;
force diwako_dui_radar_icon_priority_setting = 1;
diwako_dui_radar_icon_scale_crew = 6;
diwako_dui_radar_leadingZeroes = false;
diwako_dui_radar_namelist_hideWhenLeader = false;
diwako_dui_radar_namelist_vertical_spacing = 0.75;
diwako_dui_radar_occlusion_fade_in_time = 1;
diwako_dui_radar_occlusion_fade_time = 10;
diwako_dui_radar_pointer_color = [1,0.5,0,1];
diwako_dui_radar_pointer_style = "standard";
diwako_dui_radar_show_cardinal_points = true;
diwako_dui_radar_showSpeaking = true;
diwako_dui_radar_showSpeaking_radioOnly = false;
diwako_dui_radar_showSpeaking_replaceIcon = true;
force diwako_dui_radar_sortType = "none";
force diwako_dui_radar_sqlFirst = false;
force diwako_dui_radar_syncGroup = false;
force diwako_dui_radar_vehicleCompassEnabled = false;
diwako_dui_use_layout_editor = false;

// F/A-18
force force js_jc_fa18_advancedStart = true;
js_jc_fa18_canopyLoss = false;
js_jc_fa18_cursorSensitivity = 2.5;
js_jc_fa18_interactCursor = false;
js_jc_fa18_interactionRadiusMod = 1;
js_jc_fa18_showLabels = true;

// GRAD Trenches
force force grad_trenches_functions_allowBigEnvelope = true;
force force grad_trenches_functions_allowCamouflage = true;
grad_trenches_functions_allowDigFX = false;
force force grad_trenches_functions_allowDigging = true;
force force grad_trenches_functions_allowDiggingInVehicle = true;
force force grad_trenches_functions_allowGiantEnvelope = true;
force force grad_trenches_functions_allowHitDecay = true;
grad_trenches_functions_allowHitFX = false;
force force grad_trenches_functions_allowLongEnvelope = true;
force force grad_trenches_functions_allowShortEnvelope = true;
force force grad_trenches_functions_allowSmallEnvelope = true;
force force grad_trenches_functions_allowTrenchDecay = false;
force force grad_trenches_functions_allowVehicleEnvelope = true;
force force grad_trenches_functions_bigEnvelopeDamageMultiplier = 2;
force force grad_trenches_functions_bigEnvelopeDigTime = 60;
force force grad_trenches_functions_bigEnvelopeRemovalTime = -1;
force force grad_trenches_functions_buildFatigueFactor = 0.3;
force force grad_trenches_functions_camouflageRequireEntrenchmentTool = true;
force force grad_trenches_functions_createTrenchMarker = false;
force force grad_trenches_functions_decayTime = 1800;
force force grad_trenches_functions_giantEnvelopeDamageMultiplier = 1;
force force grad_trenches_functions_giantEnvelopeDigTime = 135;
force force grad_trenches_functions_giantEnvelopeRemovalTime = -1;
force force grad_trenches_functions_hitDecayMultiplier = 1;
force force grad_trenches_functions_LongEnvelopeDigTime = 200;
force force grad_trenches_functions_LongEnvelopeRemovalTime = -1;
force force grad_trenches_functions_shortEnvelopeDamageMultiplier = 2;
force force grad_trenches_functions_shortEnvelopeDigTime = 30;
force force grad_trenches_functions_shortEnvelopeRemovalTime = -1;
force force grad_trenches_functions_smallEnvelopeDamageMultiplier = 3;
force force grad_trenches_functions_smallEnvelopeDigTime = 45;
force force grad_trenches_functions_smallEnvelopeRemovalTime = -1;
force force grad_trenches_functions_stopBuildingAtFatigueMax = false;
force force grad_trenches_functions_timeoutToDecay = 7200;
force force grad_trenches_functions_vehicleEnvelopeDamageMultiplier = 1;
force force grad_trenches_functions_vehicleEnvelopeDigTime = 240;
force force grad_trenches_functions_vehicleEnvelopeRemovalTime = -1;

// ITC Land
ITC_LAND_CIWS = true;

// LAMBS Danger
force force lambs_danger_cqbRange = 40;
force force lambs_danger_disableAIAutonomousManoeuvres = false;
force force lambs_danger_disableAIDeployStaticWeapons = false;
force force lambs_danger_disableAIFindStaticWeapons = false;
force force lambs_danger_disableAIHideFromTanksAndAircraft = true;
force force lambs_danger_disableAIPlayerGroup = true;
force force lambs_danger_disableAIPlayerGroupReaction = true;
force force lambs_danger_disableAutonomousFlares = false;
force force lambs_danger_disableAutonomousSmokeGrenades = false;
force force lambs_danger_panicChance = 0.195851;

// LAMBS Danger Eventhandlers
force force lambs_eventhandlers_ExplosionEventHandlerEnabled = true;
force force lambs_eventhandlers_ExplosionReactionTime = 9;

// LAMBS Danger WP
force force lambs_wp_autoAddArtillery = true;

// LAMBS Main
force force lambs_main_combatShareRange = 100;
force force lambs_main_debug_drawAllUnitsInVehicles = false;
force force lambs_main_debug_Drawing = false;
force force lambs_main_debug_FSM = false;
force force lambs_main_debug_FSM_civ = false;
force force lambs_main_debug_functions = false;
force force lambs_main_debug_RenderExpectedDestination = false;
lambs_main_disableAICallouts = false;
lambs_main_disableAIDodge = false;
lambs_main_disableAIFleeing = false;
lambs_main_disableAIGestures = false;
lambs_main_disableAutonomousMunitionSwitching = false;
lambs_main_disablePlayerGroupSuppression = true;
force force lambs_main_indoorMove = 0.152807;
force force lambs_main_maxRevealValue = 0.75;
force force lambs_main_minFriendlySuppressionDistance = 5;
force force lambs_main_minObstacleProximity = 5;
force force lambs_main_minSuppressionRange = 70;
force force lambs_main_radioBackpack = 1000;
lambs_main_radioDisabled = false;
force force lambs_main_radioEast = 350;
force force lambs_main_radioGuer = 350;
force force lambs_main_radioShout = 50;
force force lambs_main_radioWest = 350;

// MCC Classes
force MCC_convoyHVT = "[[""None"",""0""],[""B.Officer"",""B_officer_F""],[""B. Pilot"",""B_Helipilot_F""],[""O. Officer"",""O_officer_F""],[""O. Pilot"",""O_helipilot_F""],[""I.Commander"",""I_officer_F""],[""Citizen"",""C_man_polo_1_F""],[""C.Pilot"",""C_man_pilot_F""],[""Orestes"",""C_Orestes""],[""Nikos"",""C_Nikos""],[""Hunter"",""C_man_hunter_1_F""],[""Kerry"",""I_G_Story_Protagonist_F""]]";
force MCC_convoyHVTcar = "[[""Default"",""""],[""Hunter"",""B_Hunter_F""],[""MRAP"",""I_MRAP_03_F""],[""Quadbike"",""B_Quadbike_F""],[""Ifrit"",""O_Ifrit_F""],[""Offroad"",""C_Offroad_01_F""],[""SUV"",""C_SUV_01_F""],[""Hatchback"",""C_Hatchback_01_F""]]";
force MCC_ied_hidden = "[[""Dirt Small"",""IEDLandSmall_Remote_Ammo""],[""Dirt Big"",""IEDLandBig_Remote_Ammo""],[""Urban Small"",""IEDUrbanSmall_Remote_Ammo""],[""Urban Big"",""IEDUrbanBig_Remote_Ammo""]]";
force MCC_ied_medium = "[[""Wheel Cart"",""Land_WheelCart_F""],[""Metal Barrel"",""Land_MetalBarrel_F""],[""Plastic Barrel"",""Land_BarrelSand_F""],[""Pipes"",""Land_Pipes_small_F""],[""Wooden Crates"",""Land_CratesShabby_F""],[""Wooden Box"",""Land_WoodenBox_F""],[""Cinder Blocks"",""Land_Ytong_F""],[""Sacks Heap"",""Land_Sacks_heap_F""], [""Water Barrel"",""Land_WaterBarrel_F""],[""Water Tank"",""Land_WaterTank_F""]]";
force MCC_ied_small = "[[""Plastic Crates"",""Land_CratesPlastic_F""],[""Plastic Canister"",""Land_CanisterPlastic_F""],[""Sack"",""Land_Sack_F""],[""Road Cone"",""RoadCone""],[""Tyre"",""Land_Tyre_F""],[""Radio"",""Land_SurvivalRadio_F""],[""Suitcase"",""Land_Suitcase_F""],[""Grinder"",""Land_Grinder_F""],[""MultiMeter"",""Land_MultiMeter_F""],[""Plastic Bottle"",""Land_BottlePlastic_V1_F""],[""Fuel Canister"",""Land_CanisterFuel_F""],[""FM Radio"",""Land_FMradio_F""],[""Camera"",""Land_HandyCam_F""],[""Laptop"",""Land_Laptop_F""],[""Mobile Phone"",""Land_MobilePhone_old_F""],[""Smart Phone"",""Land_MobilePhone_smart_F""],[""Longrange Radio"",""Land_PortableLongRangeRadio_F""],[""Satellite Phone"",""Land_SatellitePhone_F""],[""Money"",""Land_Money_F""]]";
force MCC_ied_wrecks = "[[""MI-48"",""Land_UWreck_Heli_Attack_02_F""],[""BMP2"",""Land_Wreck_BMP2_F""],[""Car"",""Land_Wreck_Car_F""],[""Car2"",""Land_Wreck_Car2_F""],[""Car Dismantled"",""Land_Wreck_CarDismantled_F""],[""Blackfoot"",""Land_Wreck_Heli_Attack_01_F""],[""HMMW"",""Land_Wreck_HMMWV_F""],[""Hunter"",""Land_Wreck_Hunter_F""],[""Offroad2"",""Land_Wreck_Offroad2_F""],[""Skoda"",""Land_Wreck_Skodovka_F""],[""Slammer"",""Land_Wreck_Slammer_F""],[""T72"",""Land_Wreck_T72_hull_F""],[""Truck"",""Land_Wreck_Truck_dropside_F""],[""Truck2"",""Land_Wreck_Truck_F""],[""UAZ"",""Land_Wreck_UAZ_F""],[""Van"",""Land_Wreck_Van_F""],[""Car Wreck"",""Land_Wreck_Car3_F""],[""BRDM Wreck"",""Land_Wreck_BRDM2_F""],[""Offroad Wreck"",""Land_Wreck_Offroad_F""],[""Truck Wreck"",""Land_Wreck_Truck_FWreck""]]";
force MCC_initConstract_aa = "[""O_static_AA_F"",""B_static_AA_F"",""O_static_AA_F"",""O_static_AA_F""]";
force MCC_initConstract_at = "[""O_static_AT_F"",""B_static_AT_F"",""O_static_AT_F"",""O_static_AT_F""]";
force MCC_initConstract_bunker = "[""Land_BagBunker_Small_F"",""Land_BagBunker_Small_F"",""Land_BagBunker_Small_F"",""Land_BagBunker_Small_F""]";
force MCC_initConstract_gmg = "[""O_GMG_01_F"",""B_GMG_01_F"",""O_GMG_01_F"",""O_GMG_01_F""]";
force MCC_initConstract_gmgh = "[""O_GMG_01_high_F"",""B_GMG_01_high_F"",""O_GMG_01_high_F"",""O_GMG_01_high_F""]";
force MCC_initConstract_hmg = "[""O_HMG_01_F"",""B_HMG_01_F"",""O_HMG_01_F"",""O_HMG_01_F""]";
force MCC_initConstract_hmgh = "[""O_HMG_01_F"",""B_HMG_01_high_F"",""O_HMG_01_F"",""O_HMG_01_F""]";
force MCC_initConstract_mortar = "[""O_Mortar_01_F"",""B_Mortar_01_F"",""O_Mortar_01_F"",""O_Mortar_01_F""]";
force MCC_initConstract_wall = "[""Land_BagFence_Long_F"",""Land_HBarrier_3_F"",""Land_BagFence_Long_F"",""Land_BagFence_Long_F""]";

// MCC Commander Console
force MCC_allowConsole = false;
force MCC_allowRTS = false;
force MCC_ConsoleACTime = 300;
force MCC_ConsoleCanCommandAI = false;
force MCC_ConsoleLiveFeedHelmets = "[""H_HelmetB"",""H_HelmetB_paint"",""H_HelmetB_light"",""H_HelmetO_ocamo"",""H_HelmetLeaderO_ocamo"",""H_HelmetSpecO_ocamo"",""H_HelmetSpecO_blk""]";
force MCC_ConsoleLiveFeedHelmetsOnly = false;
force MCC_ConsoleOnlyShowUnitsWithGPS = false;
force MCC_ConsolePlayersCanSeeWPonMap = false;
force MCC_defaultCASEnabled = false;
force MCC_defaultSupplyDropsEnabled = false;

// MCC GAIA
force GAIA_CACHE_STAGE_1 = 10000;
force MCC_AI_AimIndex = 5.45;
force MCC_AI_CommandIndex = 10;
force MCC_AI_Skill_Index = 10;
force MCC_AI_SpotIndex = 6.83032;
force MCC_GAIA_AMBIANT = true;
force MCC_GAIA_AMBIANT_CHANCE = 80;
force MCC_GAIA_ATTACKS_FOR_NONGAIA = false;

// MCC General
force MCC_allowlogistics = false;
force MCC_allowsqlPDA = true;
force force MCC_ambientBirdsSettingIndex = false;
force force MCC_ambientFireSettingIndex = false;
force MCC_armedCivilansWeapons = "[""hgun_P07_F"",""hgun_Rook40_F"",""hgun_ACPC2_F"",""hgun_Pistol_heavy_01_F"",""hgun_Pistol_heavy_02_F"",""SMG_01_F"",""SMG_02_F"",""hgun_PDW2000_F""]";
force MCC_artilleryComputerIndex = true;
force MCC_artillerySpreadArray = "[[""On-target"",10], [""Precise"",100], [""Tight"",200], [""Wide"",400]]";
MCC_CuratorEditDisabled = false;
force MCC_fnc_ambientFireInitCrewBurnChance = 50;
force MCC_fnc_ambientFireInitCrewNewFireChance = 10;
force MCC_fnc_ambientFireInitExplosivesBurnChance = 3;
force MCC_fnc_ambientFireInitVehicleBurnChance = 50;
force MCC_MessagesIndex = false;
force MCC_syncOn = false;
force MCC_t2tIndex = 0;
force MCC_timeMultiplier_settings = 1;

// MCC Mechanics
force MCC_allow3DSpotting = false;
force MCC_arcadeTanks = false;
force MCC_breacingAmmo = "[""prpl_8Rnd_12Gauge_Slug"",""prpl_6Rnd_12Gauge_Slug"",""rhsusf_8Rnd_Slug"",""rhsusf_5Rnd_Slug""]";
MCC_cover = false;
MCC_coverUI = false;
MCC_coverVault = false;
force MCC_disableFatigue = true;
force MCC_ingameUI = true;
force MCC_interaction = true;
force MCC_nonLeathalAmmo = "[""prpl_8Rnd_12Gauge_Slug"",""prpl_6Rnd_12Gauge_Slug"",""rhsusf_8Rnd_Slug"",""rhsusf_5Rnd_Slug""]";
MCC_quickWeaponChange = false;
force MCC_surviveModAllowSearch_index = 0;
force MCC_surviveModPlayerGear = false;
force MCC_surviveModPlayerPos = false;

// MCC Mission Generator
force MCC_MWAA = "[""B_APC_Tracked_01_AA_F"",""O_APC_Tracked_02_AA_F"",""I_APC_Wheeled_03_cannon_F""]";
force MCC_MWAir = "[""O_Heli_Attack_02_F"",""O_Heli_Attack_02_black_F"",""O_UAV_02_F"",""O_UAV_02_CAS_F"",""B_Heli_Attack_01_F"",""I_Plane_Fighter_03_CAS_F"",""I_Plane_Fighter_03_AA_F""]";
force MCC_MWArtillery = "[""B_MBT_01_arty_F"",""B_MBT_01_mlrs_F"",""O_MBT_02_arty_F"",""O_Mortar_01_F"",""I_Mortar_01_F""]";
force MCC_MWcache = "[""Box_East_AmmoVeh_F"",""Land_Pallet_MilBoxes_F""]";
force MCC_MWFuelTanks = "[""Land_dp_smallTank_F"",""Land_ReservoirTank_V1_F"",""Land_dp_bigTank_F""]";
force MCC_MWHVT = "[""B_officer_F"",""O_officer_F"",""I_officer_F"",""C_Nikos""]";
force MCC_MWIED = "[""IEDLandSmall_Remote_Ammo"",""IEDLandBig_Remote_Ammo"",""IEDUrbanSmall_Remote_Ammo"",""IEDUrbanBig_Remote_Ammo""]";
force MCC_MWIntelObjects = "[""Land_File2_F"",""Land_FilePhotos_F"",""Land_Laptop_unfolded_F"",""Land_SatellitePhone_F"",""Land_Suitcase_F""]";
force MCC_MWradar = "[""Land_Radar_Small_F"",""B_Radar_System_01_F"",""O_Radar_System_02_F""]";
force MCC_MWRadio = "[""Land_TTowerBig_2_F""]";
force MCC_MWSITES = "[[""Guerrilla"",""Camps"",""CampA""],[""Guerrilla"",""Camps"",""CampB""],[""Guerrilla"",""Camps"",""CampC""],[""Guerrilla"",""Camps"",""CampD""],[""Guerrilla"",""Camps"",""CampE""],[""Guerrilla"",""Camps"",""CampF""],[""MCC_comps"",""civilians"",""slums""],[""MCC_comps"",""Guerrilla"",""campSite""]]";
force MCC_MWSITESmilitary = "[[""Military"",""Outposts"",""OutpostA""],[""Military"",""Outposts"",""OutpostB""],[""Military"",""Outposts"",""OutpostC""],[""Military"",""Outposts"",""OutpostD""],[""Military"",""Outposts"",""OutpostE""],[""Military"",""Outposts"",""OutpostF""]]";
force MCC_MWTanks = "[""B_MBT_01_cannon_F"",""O_MBT_02_cannon_F""]";

// MCC Radio
force MCC_VonRadio = false;
force MCC_vonRadioDistanceCommander = 12000;
force MCC_vonRadioDistanceGlobal = 2000;
force MCC_vonRadioDistanceGroup = 30;
force MCC_vonRadioDistanceSide = 5000;
force MCC_vonRadioKickIdle = false;

// MCC Respawn
force MCC_deletePlayersBody = false;
force MCC_openRespawnMenu = false;
force MCC_respawnCinematic = false;
force MCC_respawnOnGroupLeader = false;
force MCC_saveGearIndex = false;

// MCC Role Selection
force CP_activated = false;
force CP_defaultGroups = "[""Alpha"",""Bravo"",""Charlie"",""Delta""]";
force CP_expNotifications = false;
force CP_XPperLevel = 4000;
force MCC_allowChangingKits = false;
force MCC_rsAllWeapons = false;
force MCC_rsEnableDriversPilots = false;
force MCC_rsEnableRoleWeapons = false;

// MCC User Interface
force MCC_groupMarkers = false;
force force MCC_hitRadar = 2;
force MCC_indevidualMarkers = false;
force MCC_nameTags = false;
force MCC_NameTags_direct = false;
force MCC_Settings_compassEnabled = 2;
force MCC_Settings_forceCamera = 3;
MCC_Settings_grassDetail = 10;
MCC_Settings_ViewDistance = 5000;
force MCC_showActionKey = true;
force force MCC_suppressionOn = false;
force MCC_UIModuleTickets = false;

// MRHSatellite Options
force MRH_SAT_allowFullscreen = true;
force MRH_SAT_allowLasering = true;
force MRH_SAT_allowTargetDetection = true;
force MRH_SAT_allowTargetTracking = true;
force MRH_SAT_MaxSatAltitude = 300;

// Necroplague
force dev_cba_infection = true;
dev_cba_infection_prolongTime = 900;
dev_cba_infection_resurrectAmugusChance = 0;
dev_cba_infection_resurrectParasiteChance = 0.1;
dev_cba_infection_resurrectTime = "[25,30,35]";
force dev_cba_infection_resurrectWebknight = false;
force dev_cba_infection_resurrectZombie = true;
dev_cba_infection_totalTime = 180;

// Necroplague - Infected
dev_zombie_attack_damageMn = 0.125;
dev_zombie_attack_damageVeh = 0.001;
dev_zombie_attack_launchVeh = "[0,1,1]";
dev_zombie_attack_reachMan = 3.5;
dev_zombie_attack_reachVeh = 8;
dev_zombie_attack_timeout = 0.5;
dev_zombie_distance_agro = 50;
dev_zombie_distance_hunt = 100;
dev_zombie_distance_max = 500;
dev_zombie_distance_roam = 75;
force dev_zombie_greenEyes = true;
dev_zombie_head_caliber = 9;
dev_zombie_head_chance = 0.5;
dev_zombie_health = 1;
dev_zombie_infectionChance = 0.5;
force dev_zombie_uniformFix = false;
force dev_zombie_useGlasses = false;
force dev_zombie_useIdentity = true;

// Necroplague - Spitter
dev_toxmut_attack_damageMan = 0.3;
dev_toxmut_attack_damageManSpit = 0.5;
dev_toxmut_attack_damageVeh = 0.01;
dev_toxmut_attack_launchVeh = "[0,1,0.1]";
dev_toxmut_attack_reachMan = 3.5;
dev_toxmut_attack_reachSpit = 30;
dev_toxmut_attack_reachVeh = 8;
dev_toxmut_attack_specialChance = 0.5;
dev_toxmut_attack_timeout = 0.5;
dev_toxmut_attack_timeoutSpit = 15;
dev_toxmut_distance_agro = 75;
dev_toxmut_distance_hunt = 300;
dev_toxmut_distance_max = 1000;
dev_toxmut_distance_roam = 75;
dev_toxmut_health = 1;

// Necroplague - Stalker
dev_form939_attack_damageMan = 0.5;
dev_form939_attack_damageVeh = 0.01;
dev_form939_attack_launchVeh = "[0,1,1]";
dev_form939_attack_psychChance = 0.1;
dev_form939_attack_psychDuration = 15;
dev_form939_attack_psychTimeout = 30;
dev_form939_attack_reachMan = 5;
dev_form939_attack_reachVeh = 8;
dev_form939_attack_timeout = 0.5;
dev_form939_distance_agro = 100;
dev_form939_distance_hunt = 1400;
dev_form939_distance_max = 1500;
dev_form939_distance_roam = 75;
dev_form939_health = 0.11;

// Necroplague - The Bully
dev_asymhuman_attack_bigArmChance = 0.5;
dev_asymhuman_attack_damageMan = 0.5;
dev_asymhuman_attack_damageVeh = 0.2;
dev_asymhuman_attack_infectionChance = 0.5;
dev_asymhuman_attack_launchMan = "[0,6,5]";
dev_asymhuman_attack_launchVeh = "[0,10,5]";
dev_asymhuman_attack_reachMan = 3.5;
dev_asymhuman_attack_reachVeh = 8;
dev_asymhuman_attack_timeout = 0.5;
dev_asymhuman_distance_agro = 75;
dev_asymhuman_distance_hunt = 300;
dev_asymhuman_distance_max = 1000;
dev_asymhuman_distance_roam = 75;
dev_asymhuman_health = 1;

// Necroplague - The Hivemind
dev_hivemind_hallucinate2Distance = 50;
dev_hivemind_hallucinateProbabilityClose = 20;
dev_hivemind_hallucinateProbabilityFar = 20;
dev_hivemind_health = 0.2;
dev_hivemind_maxCooldown = 40;
dev_hivemind_maxDistance = 150;
dev_hivemind_minCooldown = 15;
dev_hivemind_suicideTime = 180;

// Necroplague - The Jumper
dev_asymhuman_stage2_agroDistance = 50;
dev_asymhuman_stage2_attack2Timeout = 10;
dev_asymhuman_stage2_attackDistanceMan = 2.5;
dev_asymhuman_stage2_attackDistanceVeh = 8;
dev_asymhuman_stage2_attackTimeout = 1;
dev_asymhuman_stage2_damageMan = 0.2;
dev_asymhuman_stage2_damageManAttack2 = 0.3;
dev_asymhuman_stage2_damageVeh = 0.1;
dev_asymhuman_stage2_distance_roam = 50;
dev_asymhuman_stage2_health = 0.1;
dev_asymhuman_stage2_huntDistance = 75;
dev_asymhuman_stage2_infectionChance = 0.5;
dev_asymhuman_stage2_jumpTimeout = 30;
dev_asymhuman_stage2_maxDistance = 500;

// Necroplague - The Parasite
dev_parasite_agroDistance = 1000;
dev_parasite_attack2Timeout = 4;
dev_parasite_attack3Timeout = 20;
dev_parasite_attackDistanceMan = 3.5;
dev_parasite_attackDistanceVeh = 7;
dev_parasite_attackTimeout = 1;
dev_parasite_damageMan = 0.2;
dev_parasite_damageManAttack2 = 0.2;
dev_parasite_damageManAttack3 = 0.25;
dev_parasite_damageVeh = 0.001;
dev_parasite_distance_roam = 75;
dev_parasite_health = 0.05;
dev_parasite_huntDistance = 1500;
dev_parasite_infectionChance = 1;
dev_parasite_jumpTimeout = 20;
dev_parasite_maxDistance = 2000;
dev_parasite_specialChance = 0.1;

// No More Aircraft Bouncing
force NMAB_setting_classExclusions = "";
NMAB_setting_pfxHelicopters = true;
NMAB_setting_pfxPlanes = true;

// OPAEX Settings
OPAEX_D20_Debug_ForcedSelection = "";
OPAEX_D20_Debug_Mode = false;
force OPAEX_D20_Enabled = false;
force OPAEX_SkeletonSwap_Enabled = true;

// OPTRE Powered MJOLNIR
force OPTRE_HELMET_HUD_EVA = "OPTRE_MJOLNIR_EVAHelmet,OPTRE_MJOLNIR_EVAHelmet_Emily";
force OPTRE_HELMET_HUD_MARKIV = "OPTRE_MJOLNIR_Mk4Helmet,OPTRE_MJOLNIR_Mk4Helmet_Blue,OPTRE_MJOLNIR_Mk4Helmet_Red";
force OPTRE_HELMET_HUD_MARKV = "OPTRE_FC_MJOLNIR_MKV_Helmet,OPTRE_FC_MJOLNIR_MKV_Helmet_Black,OPTRE_FC_MJOLNIR_MKV_Helmet_117,OPTRE_FC_MJOLNIR_MKV_Helmet_Caboose,OPTRE_FC_MJOLNIR_MKV_Helmet_Freeman,OPTRE_FC_MJOLNIR_MKV_Helmet_Church,OPTRE_FC_MJOLNIR_MKV_Helmet_Donut,OPTRE_FC_MJOLNIR_MKV_Helmet_Simmons,OPTRE_FC_MJOLNIR_MKV_Helmet_Night,OPTRE_FC_MJOLNIR_MKV_Helmet_Olive,OPTRE_FC_MJOLNIR_MKV_Helmet_Grif,OPTRE_FC_MJOLNIR_MKV_Helmet_Sarge,OPTRE_FC_MJOLNIR_MKV_Helmet_Tucker";
force OPTRE_HELMET_HUD_MARKVB = "OPTRE_MJOLNIR_MkVBHelmet";
force OPTRE_HELMET_HUD_MARKVI = "OPTRE_FC_MJOLNIR_Mark_VI_Helmet,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_White,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Olive,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Tan,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Tex,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Caboose,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Church,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Donut,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Grif,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Simmons,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Sarge,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Kaikaina,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Lopez,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Doc,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_North,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_South,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_York,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Washington,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Tucker";
force OPTRE_HELMET_HUD_MP = "OPTRE_MJOLNIR_MPHelmet";
force OPTRE_HELMET_HUD_RECON = "OPTRE_MJOLNIR_ReconHelmet";
force OPTRE_HELMET_HUD_SANGHEILI = "OPTRE_FC_Elite_Helmet_FieldMarshal,OPTRE_FC_Elite_Helmet_HonorGuard_Ultra,OPTRE_FC_Elite_Helmet_HonorGuard,OPTRE_FC_Elite_Helmet_Major,OPTRE_FC_Elite_Helmet_Minor,OPTRE_FC_Elite_Helmet_SpecOps,OPTRE_FC_Elite_Helmet_Ultra,OPTRE_FC_Elite_Helmet_Zealot";
OPTRE_HUD_SHIELD_BAR_HEIGHT = "0.057079";
OPTRE_HUD_SHIELD_BAR_POS_X = "0.396875";
OPTRE_HUD_SHIELD_BAR_POS_Y = "0.055897";
force OPTRE_HUD_SHIELD_BAR_TEXTURE = "\A3\ui_f\data\GUI\RscCommon\RscProgress\progressbar_ca.paa";
OPTRE_HUD_SHIELD_BAR_WIDTH = "0.206250";
force OPTRE_HUD_TEXTURE_EVA = "\OPTRE_Suit_Scripts\textures\EVA_Hud_No_Crosshair.paa";
force OPTRE_HUD_TEXTURE_MARKIV = "\OPTRE_Suit_Scripts\textures\MarkIV_Hud_No_Crosshair.paa";
force OPTRE_HUD_TEXTURE_MARKV = "\OPTRE_Suit_Scripts\textures\MarkV_Hud_No_Crosshair.paa";
force OPTRE_HUD_TEXTURE_MARKVB = "\OPTRE_Suit_Scripts\textures\MarkVI_Hud_No_Crosshair.paa";
force OPTRE_HUD_TEXTURE_MARKVI = "\OPTRE_Suit_Scripts\textures\MarkVI_Hud_No_Crosshair.paa";
force OPTRE_HUD_TEXTURE_MP = "\OPTRE_Suit_Scripts\textures\MP_Hud_No_Crosshair.paa";
force OPTRE_HUD_TEXTURE_RECON = "\OPTRE_Suit_Scripts\textures\RECON_Hud_No_Crosshair.paa";
force OPTRE_HUD_TEXTURE_SANGHEILI = "\OPTRE_Suit_Scripts\textures\Sangheili_Hud_No_Crosshair.paa";
force OPTRE_JUMP_SUITS_SETTING = "OPTRE_MJOLNIR_Undersuit,OPTRE_MJOLNIR_Dress_Uniform,OPTRE_FC_MJOLNIR_MKVI_Undersuit,OPTRE_FC_Elite_CombatSkin";
force OPTRE_MJOLNIR_ACTIVATE_AI = true;
OPTRE_MJOLNIR_CAMSHAKE = true;
force OPTRE_MJOLNIR_ENABLE_JUMP = true;
force OPTRE_MJOLNIR_ENABLE_SPARKS_HIT = true;
force OPTRE_MJOLNIR_ENABLE_SPARKS_SHIELD = true;
OPTRE_MJOLNIR_ENERGY_BAR_ACTIVE = true;
OPTRE_MJOLNIR_ENERGY_BAR_COLOR = [0.5,0.9,0.9,0.8];
OPTRE_MJOLNIR_HUD_ACTIVE_INTRO = true;
OPTRE_MJOLNIR_HUD_OVERLAY = true;
force OPTRE_MJOLNIR_INCREASED_SPEED = true;
force OPTRE_MJOLNIR_JUMP_FORWARD = 3;
force OPTRE_MJOLNIR_JUMP_UP_HIGH = 3;
force OPTRE_MJOLNIR_JUMP_UP_LOW = 5;
force OPTRE_MJOLNIR_PREVENT_FALLDAMAGE = true;
force OPTRE_MJOLNIR_RECOIL_MODIFIER = 0.3;
force OPTRE_MJOLNIR_SHIELD_ENERGY = 75;
force OPTRE_MJOLNIR_SHIELD_ENERGY_AI = 250;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_1 = 1;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_2 = 1.5;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_3 = 2;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_4 = 2.5;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_5 = 3;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_1 = "OPTRE_FC_Elite_Armor_Minor";
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_2 = "OPTRE_FC_Elite_Armor_Major,OPTRE_FC_Elite_Armor_SpecOps";
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_3 = "OPTRE_FC_Elite_Armor_Ultra";
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_4 = "OPTRE_FC_Elite_Armor_Zealot,OPTRE_FC_Elite_Armor_FieldMarshal";
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_5 = "OPTRE_FC_Elite_Armor_HonorGuard,OPTRE_FC_Elite_Armor_HonorGuard_Ultra";
force OPTRE_MJOLNIR_SHIELD_REGEN = 1;
force OPTRE_MJOLNIR_SHIELD_REGEN_AI = 0.5;
force OPTRE_MJOLNIR_SHOW_ACTIVATE = true;
OPTRE_MJOLNIR_SHOW_DEACTIVATE = false;
force OPTRE_MJOLNIR_SPEED_MODIFIER = 1.1;
force OPTRE_MJOLNIR_SUPPRESS_RECOIL = true;
force OPTRE_POWERED_SUITS_SETTING = "OPTRE_MJOLNIR_Mk4Armor,OPTRE_MJOLNIR_Mk4Armor_Blue,OPTRE_MJOLNIR_Mk4Armor_Red,OPTRE_FC_MJOLNIR_MKV_Armor,OPTRE_FC_MJOLNIR_MKV_Armor_Black,OPTRE_FC_MJOLNIR_MKV_Armor_Caboose,OPTRE_FC_MJOLNIR_MKV_Armor_Freeman,OPTRE_FC_MJOLNIR_MKV_Armor_Church,OPTRE_FC_MJOLNIR_MKV_Armor_Donut,OPTRE_FC_MJOLNIR_MKV_Armor_Simmons,OPTRE_FC_MJOLNIR_MKV_Armor_Night,OPTRE_FC_MJOLNIR_MKV_Armor_Olive,OPTRE_FC_MJOLNIR_MKV_Armor_Grif,OPTRE_FC_MJOLNIR_MKV_Armor_Sarge,OPTRE_FC_MJOLNIR_MKV_Armor_Tucker,OPTRE_MJOLNIR_MkVBArmor,OPTRE_FC_MJOLNIR_Mark_VI_Armor,MJOLNIR_Mark_VI_Armour_Caboose,MJOLNIR_Mark_VI_Armour_Church,MJOLNIR_Mark_VI_Armour_Doc,MJOLNIR_Mark_VI_Armour_Desert,MJOLNIR_Mark_VI_Armour_Donut,MJOLNIR_Mark_VI_Armour_Grif,MJOLNIR_Mark_VI_Armour_Kaikaina,MJOLNIR_Mark_VI_Armour_Lopez,MJOLNIR_Mark_VI_Armour_North,MJOLNIR_Mark_VI_Armour_Sarge,MJOLNIR_Mark_VI_Armour_Simmons,MJOLNIR_Mark_VI_Armour_Snow,MJOLNIR_Mark_VI_Armour_South,MJOLNIR_Mark_VI_Armour_Tucker,MJOLNIR_Mark_VI_Armour_Urban,MJOLNIR_Mark_VI_Armour_Washington,MJOLNIR_Mark_VI_Armour_Woodland,MJOLNIR_Mark_VI_Armour_York,OPTRE_FC_Elite_Armor_FieldMarshal,OPTRE_FC_Elite_Armor_HonorGuard_Ultra,OPTRE_FC_Elite_Armor_HonorGuard,OPTRE_FC_Elite_Armor_Major,OPTRE_FC_Elite_Armor_Minor,OPTRE_FC_Elite_Armor_SpecOps,OPTRE_FC_Elite_Armor_Ultra,OPTRE_FC_Elite_Armor_Zealot,MEU_Elite_Armor_Minor,MEU_Elite_Armor_Major,MEU_Elite_Armor_SpecOps,MEU_Elite_Armor_Ultra,MEU_Elite_Armor_Zealot,MEU_Elite_Armor_FieldMarshal,MEU_Elite_Armor_HonorGuard,MEU_Elite_Armor_HonorGuard_Ultra";

// Task Force Arrowhead Radio
TF_default_radioVolume = 9;
force TF_give_microdagr_to_soldier = true;
force TF_give_personal_radio_to_regular_soldier = false;
force TF_no_auto_long_range_radio = true;
force TF_same_dd_frequencies_for_side = false;
force TF_same_lr_frequencies_for_side = false;
force TF_same_sw_frequencies_for_side = false;

// TTS Beam Laser
force tts_beam_cleanupSkeletons = false;
force tts_beam_cleanupSkeletonsDelay = 300;
force tts_beam_createCraters = true;
tts_beam_disableImpactFlash = false;
force tts_beam_structureFireChance = 0.03;
force tts_beam_structureFireMaxDuration = 120;
force tts_beam_structureFireMinDuration = 60;
force tts_beam_structureFiresEnabled = true;
force tts_beam_vaporiseBodies = true;

// ZaD - ACE integration
force force zad_ace_int_pillHeal_enable = true;

// Zeus Additions
zeus_additions_main_AABLU_mags = "[""rhs_fim92_mag"",""Titan_AA""]";
zeus_additions_main_AARED_mags = "[""rhs_mag_9k38_rocket"",""Titan_AA""]";
zeus_additions_main_blacklistFKEnable = false;
zeus_additions_main_blacklistSettings = "[]";
zeus_additions_main_enableACECargoHint = true;
zeus_additions_main_enableACEDragHint = true;
zeus_additions_main_enableACEMedicalHint = true;
zeus_additions_main_enableExitUnconsciousUnit = true;
zeus_additions_main_enableJIP = false;
zeus_additions_main_enableMissionCounter = false;
zeus_additions_main_enableRHSHint = true;
zeus_additions_main_enableSnowScriptHint = true;
zeus_additions_main_enableTFARHint = true;
zeus_additions_main_HATBLU_mags = "[""UK3CB_BAF_Javelin_Slung_Tube"",""rhs_fgm148_magazine_AT""]";
zeus_additions_main_HATRED_mags = "[""Vorona_HE"",""Vorona_HEAT""]";
zeus_additions_main_LATBLU_mags = "[""UK3CB_BAF_AT4_CS_AP_Launcher"",""UK3CB_BAF_AT4_CS_AT_Launcher"",""rhs_weap_M136"",""rhs_weap_M136_hedp"",""rhs_weap_M136_hp"",""rhs_weap_m72a7""]";
zeus_additions_main_LATRED_mags = "[""rhs_weap_rpg18"",""rhs_weap_rpg26"",""rhs_weap_rshg2"",""rhs_weap_m80"",""rhs_weap_rpg75""]";
zeus_additions_main_MATBLU_mags = "[""rhs_mag_maaws_HE"",""rhs_mag_maaws_HEAT"",""MRAWS_HE_F"",""MRAWS_HEAT_F"",""rhs_mag_smaw_HEDP"",""rhs_mag_smaw_HEAA""]";
zeus_additions_main_MATRED_mags = "[""rhs_rpg7_OG7V_mag"",""rhs_rpg7_PG7V_mag"",""rhs_rpg7_PG7VL_mag"",""rhs_rpg7_PG7VM_mag"",""rhs_rpg7_PG7VR_mag"",""rhs_rpg7_TBG7V_mag"",""rhs_rpg7_type69_airburst_mag"",""RPG7_F"",""RPG32_HE_F"",""RPG32_F""]";

// Zeus Enhanced
zen_camera_adaptiveSpeed = true;
zen_camera_defaultSpeedCoef = 1;
zen_camera_fastSpeedCoef = 1;
zen_camera_followTerrain = true;
force zen_common_ascensionMessages = true;
force zen_common_autoAddObjects = true;
force zen_common_cameraBird = false;
zen_common_darkMode = true;
zen_common_disableGearAnim = false;
zen_common_preferredArsenal = 1;
zen_compat_ace_hideModules = true;
zen_context_menu_enabled = 2;
zen_context_menu_overrideWaypoints = false;
zen_editor_addGroupIcons = false;
zen_editor_declutterEmptyTree = true;
zen_editor_disableLiveSearch = true;
zen_editor_moveDisplayToEdge = true;
force zen_editor_parachuteSounds = true;
zen_editor_previews_enabled = true;
zen_editor_randomizeCopyPaste = false;
zen_editor_removeWatermark = true;
zen_editor_unitRadioMessages = 0;
force zen_placement_enabled = true;
zen_remote_control_cameraExitPosition = 2;
zen_visibility_enabled = true;
zen_vision_enableBlackHot = false;
zen_vision_enableBlackHotGreenCold = false;
zen_vision_enableBlackHotRedCold = false;
zen_vision_enableGreenHotCold = false;
zen_vision_enableNVG = true;
zen_vision_enableRedGreenThermal = false;
zen_vision_enableRedHotCold = false;
zen_vision_enableWhiteHot = true;
zen_vision_enableWhiteHotRedCold = false;

// Zeus Enhanced - Faction Filter
zen_faction_filter_0_dev_groups_east = true;
zen_faction_filter_0_dev_mutants = true;
zen_faction_filter_0_LM_OOPCAN_SU_groups = true;
zen_faction_filter_0_LM_OPCAN_FRI = true;
zen_faction_filter_0_LM_OPCAN_FRI_DES_groups = true;
zen_faction_filter_0_LM_OPCAN_FRI_groups = true;
zen_faction_filter_0_LM_OPCAN_FRI_WDL_groups = true;
zen_faction_filter_0_LM_OPCAN_KOS = true;
zen_faction_filter_0_LM_OPCAN_KOS_groups = true;
zen_faction_filter_0_LM_OPCAN_SU = true;
zen_faction_filter_0_LM_OPCAN_URA_groups = true;
zen_faction_filter_0_MEU_cat_A = true;
zen_faction_filter_0_MEU_Covenant = true;
zen_faction_filter_0_MEU_ER = true;
zen_faction_filter_0_MEU_Friden = true;
zen_faction_filter_0_MEU_Friden_Des = true;
zen_faction_filter_0_MEU_Friden_Wdl = true;
zen_faction_filter_0_MEU_Friden_Win = true;
zen_faction_filter_0_MEU_Koslovic_Ard = true;
zen_faction_filter_0_MEU_Koslovic_des = true;
zen_faction_filter_0_MEU_Koslovic_Jun = true;
zen_faction_filter_0_MEU_Koslovic_Swa = true;
zen_faction_filter_0_MEU_Koslovic_Urb = true;
zen_faction_filter_0_MEU_Koslovic_Wdl = true;
zen_faction_filter_0_MEU_Koslovic_Win = true;
zen_faction_filter_0_MEU_SU = true;
zen_faction_filter_0_MEU_URA = true;
zen_faction_filter_0_MEU_URA_BJ = true;
zen_faction_filter_0_MEU_URA_BJ_WDL = true;
zen_faction_filter_0_MEU_URA_D = true;
zen_faction_filter_0_MEU_URF = true;
zen_faction_filter_0_MEU_URF_BJ_URB = true;
zen_faction_filter_0_MEU_URF_BJ_WDL = true;
zen_faction_filter_0_MEU_VIP = true;
zen_faction_filter_0_MEU_Zeus_Spacer = true;
zen_faction_filter_0_OPF_F = true;
zen_faction_filter_0_OPF_G_F = true;
zen_faction_filter_0_OPF_GEN_F = true;
zen_faction_filter_0_OPF_R_F = true;
zen_faction_filter_0_OPF_T_F = true;
zen_faction_filter_0_OPTRE_FC_Covenant = true;
zen_faction_filter_0_OPTRE_Ins = true;
zen_faction_filter_0_rhs_faction_msv = true;
zen_faction_filter_0_rhs_faction_rva = true;
zen_faction_filter_0_rhs_faction_tv = true;
zen_faction_filter_0_rhs_faction_vdv = true;
zen_faction_filter_0_rhs_faction_vmf = true;
zen_faction_filter_0_rhs_faction_vpvo = true;
zen_faction_filter_0_rhs_faction_vv = true;
zen_faction_filter_0_rhs_faction_vvs = true;
zen_faction_filter_0_rhs_faction_vvs_c = true;
zen_faction_filter_0_Ryanzombiesfactionopfor = true;
zen_faction_filter_0_V_FZ_EdCat_URF = true;
zen_faction_filter_1_11th_cat_A = true;
zen_faction_filter_1_1st_meu_11th_stb_groups = true;
zen_faction_filter_1_BLU_CTRG_F = true;
zen_faction_filter_1_BLU_F = true;
zen_faction_filter_1_BLU_G_F = true;
zen_faction_filter_1_BLU_GEN_F = true;
zen_faction_filter_1_BLU_T_F = true;
zen_faction_filter_1_BLU_W_F = true;
zen_faction_filter_1_dev_groups_west = true;
zen_faction_filter_1_dev_mutants = true;
zen_faction_filter_1_LM_OPCAN_CGC = true;
zen_faction_filter_1_LM_OPCAN_CGC_WDL_groups = true;
zen_faction_filter_1_LM_OPCAN_UNSC_Army = true;
zen_faction_filter_1_LM_OPCAN_UNSC_Army_D = true;
zen_faction_filter_1_LM_OPCAN_UNSC_Army_MIX = true;
zen_faction_filter_1_LM_OPCAN_UNSC_MC = true;
zen_faction_filter_1_LM_OPCAN_UNSC_MC_CEA = true;
zen_faction_filter_1_LM_OPCAN_UNSC_MC_D = true;
zen_faction_filter_1_LM_OPCAN_UNSC_MC_INF = true;
zen_faction_filter_1_LM_OPCAN_UNSCAR_DES_groups = true;
zen_faction_filter_1_LM_OPCAN_UNSCAR_MIX_groups = true;
zen_faction_filter_1_LM_OPCAN_UNSCAR_RES_groups = true;
zen_faction_filter_1_LM_OPCAN_UNSCAR_WDL_groups = true;
zen_faction_filter_1_LM_OPCAN_UNSCMC_CEA_groups = true;
zen_faction_filter_1_LM_OPCAN_UNSCMC_DES_groups = true;
zen_faction_filter_1_LM_OPCAN_UNSCMC_INF_groups = true;
zen_faction_filter_1_LM_OPCAN_UNSCMC_WDL_groups = true;
zen_faction_filter_1_MEU_11th_stb_faction = true;
zen_faction_filter_1_MEU_cat_A = true;
zen_faction_filter_1_MEU_CPC = true;
zen_faction_filter_1_OPTRE_UNSC = true;
zen_faction_filter_1_V_FZ_EdCat_Zulu = true;
zen_faction_filter_2_ = true;
zen_faction_filter_2_11th_cat_A = true;
zen_faction_filter_2_dev_groups_indep = true;
zen_faction_filter_2_dev_mutants = true;
zen_faction_filter_2_IND_C_F = true;
zen_faction_filter_2_IND_E_F = true;
zen_faction_filter_2_IND_F = true;
zen_faction_filter_2_IND_G_F = true;
zen_faction_filter_2_IND_L_F = true;
zen_faction_filter_2_LM_OPCAN_CMA = true;
zen_faction_filter_2_LM_OPCAN_CMA_groups = true;
zen_faction_filter_2_LM_OPCAN_CPD = true;
zen_faction_filter_2_LM_OPCAN_CPD_groups = true;
zen_faction_filter_2_MEU_cat_FR = true;
zen_faction_filter_2_MEU_CMA = true;
zen_faction_filter_2_MEU_CPF = true;
zen_faction_filter_2_MEU_Oni = true;
zen_faction_filter_2_MEU_SPF = true;
zen_faction_filter_2_MEU_WR_DES = true;
zen_faction_filter_2_MEU_WR_WDL = true;
zen_faction_filter_2_OPTRE_PD = true;
zen_faction_filter_2_Ryanzombiesfaction = true;
zen_faction_filter_2_V_FZ_EdCat_CPD = true;
zen_faction_filter_2_V_FZ_EdCat_SFP = true;
zen_faction_filter_3_CIV_F = true;
zen_faction_filter_3_CIV_IDAP_F = true;
zen_faction_filter_3_dev_mutants = true;
zen_faction_filter_3_EdCat_jbad_vehicles = true;
zen_faction_filter_3_IND_L_F = true;
zen_faction_filter_3_MEU_peasants = true;
zen_faction_filter_3_OPTRE_UEG_Civ = true;
zen_faction_filter_3_V_FZ_EdCat_CIV = true;
