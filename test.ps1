param ([switch]$verbose = $false)
if($verbose -eq $true){$VerbosePreference = "Continue"}else{$VerbosePreference = "SilentlyContinue"}

$WorkshopIDs = @(
    "2789243866",#Server1
    "2789244061",#Server2
    "2789244311",#Server3
    "2789244455",#Server4
    "2789244589",#Server5
    "2789244759",#Server6
    "2789244935",#Server7
    "2789245100",#Server8
    "2789245209",#Server9
    "2789245363",#Server10
    "2794390651",#Server11
    "0",#skip12
    "0",#skip13
    "0",#skip14
    "0",#skip15
    "0",#skip16
    "0",#skip17
    "0",#skip18
    "0",#skip19
    "2789201297" #Server20
)
$i = 0
foreach($id in $WorkshopIDs)
{
    $i++
    if($id -eq "0"){continue;} #skip unpublished mods
    if(Test-Path ".\Settings\$i\cba_settings.sqf")
    {
        Write-Verbose "$i Custom Config"
    }else{
        Write-Verbose "$i Default Config"
    }
}